require 'api_constraints'

Rails.application.routes.draw do
  root 'homepage#index'
  resources :recipes, only: [:new, :create, :show, :edit, :update, :destroy] do
    member do
      post 'favorite'
      post 'share'
    end
    collection do
      get 'search'
    end
  end
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'
  get 'signup', to: 'users#new'
  resources :users, only: [:show, :create, :edit, :update] do
    member do
      get 'favorites'
    end
  end
  resources :cuisines, only: [:new, :create, :show]
  resources :food_types, only: [:new, :create, :show]
  namespace :api, defaults: { format: :json },
                              constraints: { subdomain: 'api' }, path: '/'  do
    scope module: :v1,
              constraints: ApiConstraints.new(version: 1, default: true) do
      # We are going to list our resources here
    end
  end 
end
