FactoryGirl.define do
  factory :recipe do
    sequence(:name) { |n| "#{Faker::Food.ingredient}#{n}" }
    cuisine
    food_type
    user
    serving_ammount 6
    preparation_time 30
    level Recipe.levels[:moderate]
    ingredients '300g farinha 2 ovos 1 xic açucar'
    directions 'Misturar tudo Colocar no forno a 180 graus celsius'
  end
end
