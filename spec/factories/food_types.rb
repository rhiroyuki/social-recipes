FactoryGirl.define do
  factory :food_type do
    sequence(:name) { |n| "Sobremesas#{n}"}
  end
end
