FactoryGirl.define do
  factory :user do
    sequence(:name) { |n| "Nome #{n}" }
    sequence(:mail) { |n| "email#{n}@gmail.com"}
    password  "MyString"
    password_confirmation "MyString"
    sequence(:city) { |n| "#{Faker::Address.city} #{n}"}
  end
end
