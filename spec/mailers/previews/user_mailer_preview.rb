# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  def share_recipe_mail
    UserMailer.share_recipe_mail(User.first, Recipe.first)
  end
end
