require 'rails_helper'

RSpec.describe Favorite, type: :model do
  it 'hasnt valid fields' do
    favorite = Favorite.create

    expect(favorite).to_not be_valid
  end

  it 'is valid' do
    favorite = build(:favorite)

    expect(favorite).to be_valid
  end

  it 'is invalid without user' do
    favorite = build(:favorite, user: nil)

    expect(favorite).to_not be_valid
  end

  it 'is invalid without recipe' do
    favorite = build(:favorite, recipe: nil)

    expect(favorite).to_not be_valid
  end

  describe '#most_favorited_recipes' do
    context 'has 4 favorited recipes' do
      it 'returns successfully' do
        favorites = create_list(:favorite, 4)
        expect(Favorite.most_favorited_recipes.size).to eq 4
      end
    end

    context 'has 3 favorited recipes' do
      it 'returns 3 recipes' do
        favorites = create_list(:favorite, 3)
        expect(Favorite.most_favorited_recipes.size).to eq 3
      end
    end

    context 'has 0 favoriteds recipes' do
      it 'return 0 recipes' do
        expect(Favorite.most_favorited_recipes.size).to eq 0
      end
    end

    context 'has 5 favorited recipes' do
      it 'returns 4 recipes' do
        favorites = create_list(:favorite, 5)
        expect(Favorite.most_favorited_recipes.size).to eq 4
      end
    end


  end


end
