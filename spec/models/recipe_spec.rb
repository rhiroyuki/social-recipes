require 'rails_helper'

RSpec.describe Recipe, type: :model do
  it 'should be valid' do
    recipe = Recipe.create
    expect(recipe).to_not be_valid
    expect(recipe.errors[:name]).to include("não pode ficar em branco")
    expect(recipe.errors[:cuisine]).to include("não pode ficar em branco")
    expect(recipe.errors[:food_type]).to include("é obrigatório(a)")
    expect(recipe.errors[:ingredients]).to include("não pode ficar em branco")
    expect(recipe.errors[:directions]).to include("não pode ficar em branco")
  end
end
