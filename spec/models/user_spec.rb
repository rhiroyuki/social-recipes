require 'rails_helper'

RSpec.describe User, type: :model do
  it 'should be valid' do
    user = build(:user)
    expect(user).to be_valid
  end

  it 'should have a mail' do
    user = build(:user, mail: nil)
    expect(user).to_not be_valid
  end

  it 'should have a name' do
    user = build(:user, name: nil)
    expect(user).to_not be_valid
  end

  it 'shouldnt have a long mail' do
    user = build(:user, mail: 'a'*250+'@gmail.com')
    expect(user).to_not be_valid
  end

  it 'shouldnt have a long name' do
    user = build(:user, name: 'Jonas'*50)
    expect(user).to_not be_valid
  end

  it 'should have a valid email' do
    user = build(:user, mail: 'jonas,gmail.com')
    expect(user).to_not be_valid
  end

  it 'should have a unique mail' do
    another_user = create(:user)
    user = another_user.dup
    user.mail.upcase!

    expect(user).to_not be_valid
  end

  it 'should have a valid city' do
    user = build(:user, city: nil)
    expect(user).to_not be_valid    
  end

  it 'should always save mail as downcase' do
    user = create(:user, mail: 'JONAS@GMAIL.COM')
    expect(user.mail).to eq('jonas@gmail.com')
  end

  describe 'password with minimum length' do
    it 'has 5 characters' do
      user = build(:user, password: 'a'*5, password_confirmation: 'a'*5)
      expect(user).to_not be_valid
    end

    it 'has 6 characters' do
      user = build(:user, password: 'a'*6, password_confirmation: 'a'*6)
      expect(user).to be_valid
    end
  end

  describe '#has_favorited_recipe?' do
    it 'hasnt favorited the recipe yet' do
      recipe = create(:recipe)
      user = create(:user)

      expect(user.already_favorited?(recipe)).to_not eq true
    end
    it 'has already favorited' do
      favorite = create(:favorite)
      recipe = favorite.recipe
      user = favorite.user
      expect(user.already_favorited?(recipe)).to eq true
    end
  end
end
