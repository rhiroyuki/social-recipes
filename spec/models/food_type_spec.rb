require 'rails_helper'

RSpec.describe FoodType, type: :model do
  it 'should be valid' do
    food = create(:food_type)
    expect(food).to be_valid
  end

  it 'has a unique name field' do
    another_food = create(:food_type)
    food = build(:food_type, name: another_food.name)
    food.save

    expect(food).to_not be_valid
    expect(food.errors[:name]).to include('já está em uso')
  end

  it 'has a valid name' do
    food = build(:food_type, name: nil)
    food.save
    expect(food.errors[:name]).to include('não pode ficar em branco')
  end
end
