require 'rails_helper'

RSpec.describe Cuisine, type: :model do
  it 'should be valid' do
    cuisine = build(:cuisine)

    expect(cuisine).to be_valid
  end

  it 'has a valid name' do
    cuisine = build(:cuisine, name: nil)
    cuisine.save

    expect(cuisine).to be_invalid
    expect(cuisine.errors[:name]).to include('não pode ficar em branco')
  end

  it 'has a unique name' do
    other_cuisine = create(:cuisine)
    cuisine = build(:cuisine, name: other_cuisine.name)
    cuisine.save
    
    expect(cuisine).to be_invalid
    expect(cuisine.errors[:name]).to include('já está em uso')
  end
end
