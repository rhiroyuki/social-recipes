require 'rails_helper'

feature 'user view recipes by cuisine' do
  scenario 'successfully' do
    recipe = create(:recipe)
    another_recipe = create(:recipe)

    visit root_path
    click_link recipe.cuisine.name
    expect(page).to have_content recipe.name
    expect(page).to_not have_content another_recipe.name
  end
end
