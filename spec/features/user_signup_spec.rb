require 'rails_helper'

feature 'user signs up' do
  scenario 'successfully' do
    user = build(:user)
    visit signup_path
    fill_in 'Nome', with: user.name
    fill_in 'Cidade', with: user.city
    fill_in 'Email', with: user.mail
    fill_in 'Senha', with: user.password
    fill_in 'Confirmação', with: user.password

    click_on 'Registrar'

    expect(page).to have_content('Sucesso')
  end
end
