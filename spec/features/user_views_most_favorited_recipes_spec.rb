require 'rails_helper'

feature 'view at home most favorited recipes' do
  context 'view 4 most favorited recipes at home' do
    scenario 'view 4 favorited recipes' do
      favorites = create_list(:favorite, 4)
      create_list(:recipe, 4)

      visit root_path
      within('.favorites') do
        expect(page).to have_content favorites.first.recipe.name
        expect(page).to have_content favorites.second.recipe.name
        expect(page).to have_content favorites.third.recipe.name
        expect(page).to have_content favorites.fourth.recipe.name
      end
    end
  end
end
