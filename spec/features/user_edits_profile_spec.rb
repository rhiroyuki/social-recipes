require 'rails_helper'

feature 'edits profile' do
  before do
    @user = create(:user)
    login_as @user
  end
  scenario 'successfully' do
    user_info = build(:user, name: 'foobar', city: 'foobar city', mail: 'foobar@gmail.com', password: 'foobar', password_confirmation: 'foobar', facebook: 'foobar', twitter: '@foobar')
    visit root_path
    click_on 'Profile'

    click_on 'Editar Perfil'

    fill_in 'Nome', with: user_info.name
    fill_in 'Cidade', with: user_info.city
    fill_in 'Email', with: user_info.mail
    fill_in 'Senha', with: user_info.password
    fill_in 'Confirmação', with: user_info.password_confirmation
    fill_in 'Facebook', with: user_info.facebook
    fill_in 'Twitter', with: user_info.twitter

    click_on 'Atualizar'

    expect(page).to have_css(:h2, text: user_info.name)
    expect(page).to have_content(user_info.city)
    expect(page).to have_content(user_info.facebook)
    expect(page).to have_content(user_info.twitter)
  end

  scenario 'cant edit if not the correct user' do
    another_user = create(:user)

    visit edit_user_path(another_user)
    expect(page).to have_content("You don't have permission")
  end
end
