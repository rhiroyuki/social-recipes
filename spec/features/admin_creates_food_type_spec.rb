require 'rails_helper'

feature 'creates food type' do
  scenario 'successfully' do
    food = build(:food_type)

    visit new_food_type_path
    fill_in 'Nome', with: food.name
    click_on 'Criar Tipo de Comida'

    expect(page).to have_css('h2', text: food.name)
    expect(page).to have_content('Criado com sucesso')
  end
  scenario 'fill the fields correctly' do
    visit new_food_type_path

    click_on 'Criar Tipo de Comida'

    expect(page).to have_content('Nome não pode ficar em branco')
  end
end
