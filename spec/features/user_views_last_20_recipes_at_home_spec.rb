require 'rails_helper'

feature 'views last 20 recipes at home' do
  scenario '19 recipes' do
    recipe = create(:recipe)
    create_list(:recipe, 18, food_type: recipe.food_type, cuisine: recipe.cuisine)

    visit root_path
    expect(page).to have_content(recipe.name)
  end
  scenario '20 recipes' do
    recipe = create(:recipe)
    create_list(:recipe, 19, food_type: recipe.food_type, cuisine: recipe.cuisine)

    visit root_path
    expect(page).to have_content(recipe.name)
  end

  scenario '21 recipes' do
    recipe = create(:recipe, name: 'EITA LOUCURA')
    create_list(:recipe, 20, food_type: recipe.food_type, cuisine: recipe.cuisine)

    visit root_path
    expect(page).to_not have_content(recipe.name)
  end
end
