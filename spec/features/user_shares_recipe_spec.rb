require 'rails_helper'

feature 'user shares a recipe' do
  before do
    @user = create(:user)
    login_as @user
  end
  scenario 'successfully' do
    recipe = create(:recipe)
    visit recipe_path(recipe)

    within(".share") do
      fill_in 'Email', with: 'ricardo.hejr@gmail.com'
      click_on 'Compartilhar'
    end

    expect(page).to have_content('Enviado com sucesso')
  end
end
