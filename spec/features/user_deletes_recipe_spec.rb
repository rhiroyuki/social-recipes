require 'rails_helper'

feature 'user deletes recipe' do
  before do
    @user = create(:user)
    login_as @user
  end
  scenario 'successfully' do
    recipe = create(:recipe, user: @user)
    visit recipe_path recipe
    expect { click_link 'Excluir' }.to change(Recipe, :count).by(-1)
  end
end
