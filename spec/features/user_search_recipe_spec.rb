require 'rails_helper'

feature 'search for recipe' do
  scenario 'successfully' do
    recipe = create(:recipe)

    visit root_path
    fill_in 'Buscar', with: recipe.name
    click_on 'Efetuar Busca'

    expect(page).to have_content 'Resultado'
    expect(page).to have_content recipe.name
  end

  scenario 'find no result' do
    visit root_path
    fill_in 'Buscar', with: 'String'
    click_on 'Efetuar Busca'

    expect(page).to have_content 'Resultado'
    expect(page).to have_content 'Nada encontrado'
  end
end
