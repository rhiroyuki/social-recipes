require 'rails_helper'

feature 'create recipe' do
  before do
    @user = create(:user)
    login_as @user
  end

  scenario 'successfully' do
    cuisine = create(:cuisine)
    food_type = create(:food_type)
    recipe = build(:recipe, cuisine: cuisine, food_type: food_type, user: @user)

    visit new_recipe_path

    fill_in 'Nome',               with: recipe.name
    page.attach_file('Foto', 'spec/support/files/images/momsspaghetti.jpg')
    select recipe.cuisine.name,   from: 'Cozinha'
    select recipe.food_type.name, from: 'Tipo de Comida'
    fill_in 'Porções',            with: recipe.serving_ammount
    fill_in 'Tempo de Preparo',   with: recipe.preparation_time
    select recipe.level_i18n,     from: 'Nível'
    fill_in 'Ingredientes',       with: recipe.ingredients
    fill_in 'Instruções',         with: recipe.directions

    click_on 'Criar Receita'

    expect(page).to have_css('h2', text: recipe.name)
    expect(page).to have_xpath("//img[contains(@src,'momsspaghetti')]")
    expect(page).to have_content(recipe.cuisine.name)
    expect(page).to have_content(recipe.food_type.name)
    expect(page).to have_content(recipe.serving_ammount)
    expect(page).to have_content(recipe.preparation_time)
    expect(page).to have_content(recipe.level_i18n)
    expect(page).to have_content(recipe.ingredients)
    expect(page).to have_content(recipe.directions)
    expect(page).to have_content(recipe.user.name)
    expect(page).to have_content('Criado com sucesso')
  end

  scenario 'fill the fields correctly' do
    visit new_recipe_path

    click_on 'Criar Receita'

    expect(page).to have_content('Nome não pode ficar em branco')
    expect(page).to have_content('Cozinha não pode ficar em branco')
    expect(page).to have_content('Tipo de Comida é obrigatório')
    expect(page).to have_content('Ingredientes não pode ficar em branco')
    expect(page).to have_content('Instruções não pode ficar em branco')
  end
end
