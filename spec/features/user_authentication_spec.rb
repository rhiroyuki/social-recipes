require 'rails_helper'

feature 'user authenticates' do
  scenario 'user access login at root path' do
    visit root_path
    click_link 'Log in'
    expect(current_path).to eq login_path
  end

  scenario 'successfully' do
    user = create(:user)

    visit login_path
    fill_in 'Email', with: user.mail
    fill_in 'Senha', with: user.password
    within('main') do
      click_on 'Log in'
    end

    expect(page).to have_content('Logado com Sucesso')
  end

  scenario 'fail login' do
    visit login_path
    within('main') do
      click_on 'Log in'
    end

    expect(page).to have_content('Email ou Senha inválida')
  end
end
