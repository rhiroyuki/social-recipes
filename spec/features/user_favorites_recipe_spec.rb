require 'rails_helper'

feature 'user favorites recipe' do
  context 'user is logged' do
    before do
      @user = create(:user)
      login_as @user
    end
    scenario 'successfully favorites a recipe' do
      recipe = create(:recipe)

      visit recipe_path recipe

      expect { click_link 'Favoritar' }.to change(@user.favorites, :count).by(1)
    end

    scenario 'view favorited recipes at profile' do
      favorite = create(:favorite, user: @user)

      visit user_path @user
      click_on 'Favorites'
      expect(page).to have_content(favorite.recipe.name)
    end
  end

  context 'user is not logged' do
    scenario 'cant favorite if not logged' do
      recipe = create(:recipe)

      visit recipe_path recipe
      expect(page).to_not have_content('Favoritar')
    end
  end

end
