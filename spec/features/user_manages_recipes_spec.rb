require 'rails_helper'

feature 'user manages recipes' do
  before do
    @user = create(:user, password: 'foobar', password_confirmation: 'foobar')
    login_as @user
  end

  scenario 'successfully' do
    recipe = create(:recipe, user: @user)

    visit recipe_path recipe
    expect(page).to have_content('Editar')
    expect(page).to have_content('Excluir')
  end

  scenario 'different user' do
    recipe = create(:recipe)

    visit recipe_path recipe
    expect(page).to_not have_content('Editar')
    expect(page).to_not have_content('Excluir')
  end
end
