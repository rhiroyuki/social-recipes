require 'rails_helper'

feature 'user view recipes' do
  scenario 'successfully' do
    recipe = create(:recipe)
    another_recipe = create(:recipe, name: 'Pão de Queijo', cuisine: recipe.cuisine)

    visit root_path

    expect(page).to have_css(:h2, text: recipe.name)
    expect(page).to have_css(:h2, text: another_recipe.name)
  end
end
