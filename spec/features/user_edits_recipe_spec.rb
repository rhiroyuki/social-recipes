require 'rails_helper'

feature 'user edits recipe' do

  before do
    @user = create(:user, password: 'foobar', password_confirmation: 'foobar')
    login_as @user
  end

  scenario 'successfully' do
    recipe = create(:recipe, user: @user)
    visit recipe_path recipe
    click_on 'Editar'
    fill_in 'Nome', with: 'Macaxeira'
    click_on 'Atualizar Receita'

    expect(page).to have_css(:h2, text: 'Macaxeira')
  end

  scenario 'shouldnt allow different user' do
    recipe = create(:recipe)
    visit edit_recipe_path recipe

    expect(page).to have_content 'Please Log in'
    expect(page).to have_current_path root_path
  end
end
