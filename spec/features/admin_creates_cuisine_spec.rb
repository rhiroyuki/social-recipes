require 'rails_helper'
feature 'create cuisine' do
  scenario 'successfully' do
    cuisine = build(:cuisine)

    visit new_cuisine_path

    fill_in 'Nome', with: cuisine.name
    click_on 'Criar Cozinha'

    expect(page).to have_css(:h2, text: cuisine.name)
    expect(page).to have_content('Criado com sucesso')
  end
  scenario 'fill the fields correctly' do
    visit new_cuisine_path

    click_on 'Criar Cozinha'

    expect(page).to have_content('Nome não pode ficar em branco')
  end
end
