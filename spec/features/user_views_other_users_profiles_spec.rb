require 'rails_helper'

feature 'view other users profiles' do
  scenario 'successfully' do
    @user = create(:user, name: 'Camila')
    login_as @user

    another_user = create(:user, name: 'Jonas')
    create_list(:recipe, 2, user: another_user)

    visit user_path another_user

    expect(page).to have_content another_user.name
    expect(page).to have_content another_user.city
    expect(page).to have_content another_user.mail
    within('.recipes') do
      expect(page).to have_content another_user.recipes.first.name
      expect(page).to have_content another_user.recipes.second.name
    end
  end
end
