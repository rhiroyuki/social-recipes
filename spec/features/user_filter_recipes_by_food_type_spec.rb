require 'rails_helper'

feature 'user filter recipe by food type' do
  scenario 'successfully' do
    recipe = create(:recipe)
    visit root_path

    click_on recipe.food_type.name

    expect(page).to have_content(recipe.name)
  end
end
