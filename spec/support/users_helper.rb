module UserHelper
  def login_as(user)
    visit login_path
    fill_in 'Email', with: user.mail
    fill_in 'Senha', with: user.password
    within('main') do
      click_on 'Log in'
    end
  end
end
