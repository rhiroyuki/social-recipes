class UserMailer < ApplicationMailer
  default from: 'notifications@example.com'

  def share_recipe_mail(user, recipe)
    @user = user
    @recipe = recipe
    @url  = 'http://example.com/login'
    mail(to: @user.mail, subject: "Receita saborosa!")
  end
end
