class RecipesController < ApplicationController
  before_action :correct_user, only: [:destroy, :edit, :update]
  before_action :logged_in_user, only: [:create, :destroy, :update, :edit]
  before_action :set_collections, only:[:new, :edit]
  before_action :find_recipe, only:[:show, :edit, :destroy, :update]

  def new
    @recipe = Recipe.new
  end

  def search
    @recipes = Recipe.search_name params[:search]
  end

  def create
    @recipe = current_user.recipes.new recipe_params
    if @recipe.save
      flash[:success] = 'Criado com sucesso'
      redirect_to @recipe
    else
      flash.now[:errors] = @recipe.errors.full_messages
      set_collections
      render 'new'
    end
  end

  def show
  end

  def edit
  end

  def destroy
    @recipe.destroy
    flash[:success] = 'Sucesso'
    redirect_to root_path
  end

  def update
    if @recipe.update recipe_params
      flash.now[:success] = 'Sucesso'
      redirect_to @recipe
    else
      flash.now[:errors] = @recipe.errors.full_messages
      render 'edit'
    end
  end

  def favorite
    @recipe = Recipe.find params[:id]
    @favorite = @recipe.favorites.new user: current_user
    @favorite.save
    redirect_to @recipe
  end

  def share
    @recipe = Recipe.find params[:id]
    UserMailer.share_recipe_mail current_user, @recipe
    flash[:success] = 'Enviado com sucesso'
    redirect_to @recipe
  end

  private

    def recipe_params
      params.require(:recipe).permit(:name,
                                     :photo,
                                     :cuisine_id,
                                     :food_type_id,
                                     :serving_ammount,
                                     :preparation_time,
                                     :level,
                                     :ingredients,
                                     :directions)
    end

    def find_recipe
      @recipe = Recipe.find params[:id]
    end

    def set_collections
      @food_types = FoodType.all
      @levels = Recipe.levels_i18n
      @cuisines = Cuisine.all
    end

    def correct_user
      @recipe = current_user.recipes.find_by(id: params[:id])
      flash[:danger] = 'Please Log in'
      redirect_to(root_url) if @recipe.nil?
    end
end
