class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(mail: params[:session][:mail].downcase)
    if user && user.authenticate(params[:session][:password])
      log_in user
      flash[:success] = 'Logado com Sucesso'
      redirect_to user
    else
      flash.now[:errors] = ['Email ou Senha inválida']
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end
end
