class FoodTypesController < ApplicationController
  def new
    @food_type = FoodType.new
  end

  def create
    @food_type = FoodType.new food_type_params
    if @food_type.save
      flash[:success] = 'Criado com sucesso'
      redirect_to @food_type
    else
      flash.now[:errors] = @food_type.errors.full_messages
      render 'new'
    end
  end

  def show
    @food_type = FoodType.find params[:id]
  end

  private
    def food_type_params
      params.require(:food_type).permit(:name)
    end
end
