class UsersController < ApplicationController
  before_action :find_user, only:[:update, :edit, :favorites]
  before_action :logged_in_user, only: [:edit, :update]
  before_action :correct_user, only: [:edit, :update]
  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def update
    if @user.update_attributes user_params
      flash[:success] = 'Criado com sucesso'
      redirect_to @user
    else
      flash[:errors] = @user.errors.full_messages
      render 'edit'
    end
  end

  def edit
  end

  def create
    @user = User.new user_params
    if @user.save
      flash[:success] = 'Sucesso'
      log_in @user
      redirect_to @user
    else
      flash[:errors] = @user.errors.full_messages
      render 'new'
    end
  end

  def favorites
    @recipes = @user.favorited_recipes
  end

  private
    def find_user
      @user = User.find params[:id]
    end

    def user_params
      params.require(:user).permit(:name, :mail, :city, :password,
                                   :password_confirmation, :facebook, :twitter)
    end

    def logged_in_user
      unless logged_in?
        flash[:danger] = 'Please log in'
        redirect_to login_url
      end
    end

    def correct_user
      flash[:danger] = "You don't have permission"
      redirect_to(root_url) unless @user == current_user
    end
end
