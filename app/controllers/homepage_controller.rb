class HomepageController < ApplicationController
  def index
    @recipes = Recipe.recent_by_creation 20
    @cuisines = Cuisine.all
    @food_types = FoodType.all
    @favorite_recipes = Favorite.most_favorited_recipes
  end
end
