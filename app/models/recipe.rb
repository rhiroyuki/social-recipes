class Recipe < ApplicationRecord
  ENUM_LOCALE = 'activerecord.attributes.recipe.levels'
  enum level: [:easy, :moderate, :hard]
  belongs_to :food_type
  belongs_to :cuisine
  belongs_to :user

  has_many :favorites
  has_many :favorited_users, through: :favorites, source: :user

  has_attached_file :photo, styles: { medium: "300x300>", thumb: "100x100>" },
                    default_url: "/images/recipes/missing_recipe_photo.jpg"
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/

  validates :name,
            :cuisine,
            :ingredients,
            :directions,
            presence: true

  def self.search_name word
    where("name LIKE ?", "%#{word}%")
  end

  def self.levels_i18n
    levels.keys.map { |key| [I18n.t("#{ENUM_LOCALE}.#{key}"), key] }
  end

  def level_i18n
    I18n.t("#{ENUM_LOCALE}.#{level}")
  end

  def self.recent_by_creation n=1
    order(:created_at).last(n)
  end
end
