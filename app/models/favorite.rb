class Favorite < ApplicationRecord
  FAVORITES_RECIPES = 4
  belongs_to :user
  belongs_to :recipe

  def self.most_favorited_recipes
    select(:recipe_id, 'count(*) as favorite_count').group(:recipe_id)
    .order('favorite_count DESC').first(FAVORITES_RECIPES)
  end
end
