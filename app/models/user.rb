class User < ApplicationRecord
  VALID_EMAIL_REGEX = /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  before_save { self.mail = mail.downcase }

  has_many :recipes

  has_many :favorites
  has_many :favorited_recipes, through: :favorites, source: :recipe

  validates :password, presence: true, length: { minimum:6 }
  validates :mail, presence: true, length: { maximum: 255 }, uniqueness: { case_sensitive: false }
  validates :mail, format: { with: VALID_EMAIL_REGEX }
  validates :name, presence: true, length: { maximum: 50 }
  validates :city, presence: true

  has_secure_password

  def already_favorited? recipe
    !!favorited_recipes.find_by(id: recipe.id)
  end
end
